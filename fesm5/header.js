import { Component, EventEmitter, Output, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatGridListModule, MatInputModule, MatIconModule, MatListModule, MatMenuModule, MatNativeDateModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatTabsModule, MatTooltipModule, MatToolbarModule, MatFormFieldModule } from '@angular/material';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
        this.isAdmin = false;
        this.username = '';
        this.logoutEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.updateUsername();
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.updateUsername = /**
     * @return {?}
     */
    function () {
        /*this.auth.getUser().subscribe(res => {
              if (res) {
                this.username = res['principal']['name'];
              } else {
                this.username = '';
              }
            });*/
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.logout = /**
     * @return {?}
     */
    function () {
        /*localStorage.removeItem('auth');
            this.username = '';
            // this.logoutEmitter.emit(true);
            this.auth.logout();
            this.router.navigate(['/login']);*/
    };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-header',
                    template: "<section class=\"main-header\">\n  <h2 class=\"main-text\">Saphira<span class=\"main-subtext\">3.0</span></h2>\n  <span class=\"left-menu\">\n    <h4 class=\"user-name\">{{this.username}}</h4>\n    <p matTooltip=\"Connected\" class=\"mock-status\">\u2022</p>\n    <button class=\"menu-button\" matTooltip=\"Direct: 867-5309\" mat-icon-button>\n      <mat-icon>phone</mat-icon>\n    </button>\n    <button class=\"menu-button\" matTooltip=\"surveys@surcon.net\" mat-icon-button>\n      <mat-icon>email</mat-icon>\n    </button>\n    <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\">\n      <mat-icon>menu</mat-icon>\n    </button>\n    <mat-menu #primaryMenu=\"matMenu\">\n      <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Selection\n        </button>\n      </a>\n      <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Status\n        </button>\n      </a>\n      <br>\n      <button mat-menu-item>Account</button>\n      <button mat-menu-item>Admin View</button>\n      <button mat-menu-item (click)=\"logout()\">Logout</button>\n    </mat-menu>\n  </span>\n</section>\n",
                    styles: [".main-header{width:97vw;padding:0 1.5vw;height:45px;display:flex;justify-content:space-between;border-bottom:1px solid #333}.button-link{text-decoration:none}.left-menu{display:flex;align-items:center}.user-name{font-family:Roboto-Light;font-size:20px;font-weight:200;color:#333}.mock-status{margin:-22px 10px 0;color:green;font-size:60px;height:45px}.main-text{font-family:Roboto-Light;font-size:36px;font-weight:200;color:#333}.main-subtext{color:#777}.menu-button{transition-duration:.2s}.menu-button:hover{background-color:#ddd;opacity:.5;transition-duration:.15s}a{text-decoration:none}"]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    HeaderComponent.propDecorators = {
        logoutEmitter: [{ type: Output }]
    };
    return HeaderComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        MatAutocompleteModule,
                        MatButtonModule,
                        MatButtonToggleModule,
                        MatCardModule,
                        MatCheckboxModule,
                        MatChipsModule,
                        MatDatepickerModule,
                        MatDialogModule,
                        MatExpansionModule,
                        MatGridListModule,
                        MatInputModule,
                        MatIconModule,
                        MatListModule,
                        MatMenuModule,
                        MatNativeDateModule,
                        MatProgressBarModule,
                        MatProgressSpinnerModule,
                        MatRadioModule,
                        MatSelectModule,
                        MatSidenavModule,
                        MatSliderModule,
                        MatSlideToggleModule,
                        MatSnackBarModule,
                        MatTabsModule,
                        MatTooltipModule,
                        MatToolbarModule,
                        MatFormFieldModule
                    ],
                    exports: [
                        MatAutocompleteModule,
                        MatButtonModule,
                        MatButtonToggleModule,
                        MatCardModule,
                        MatCheckboxModule,
                        MatChipsModule,
                        MatDatepickerModule,
                        MatDialogModule,
                        MatExpansionModule,
                        MatGridListModule,
                        MatInputModule,
                        MatIconModule,
                        MatListModule,
                        MatMenuModule,
                        MatNativeDateModule,
                        MatProgressBarModule,
                        MatProgressSpinnerModule,
                        MatRadioModule,
                        MatSelectModule,
                        MatSidenavModule,
                        MatSliderModule,
                        MatSlideToggleModule,
                        MatSnackBarModule,
                        MatTabsModule,
                        MatTooltipModule,
                        MatToolbarModule,
                        MatFormFieldModule
                    ]
                },] }
    ];
    return MaterialModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var HeaderModule = /** @class */ (function () {
    function HeaderModule() {
    }
    HeaderModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MaterialModule,
                        RouterModule
                    ],
                    declarations: [HeaderComponent],
                    exports: [HeaderComponent],
                    providers: [],
                    entryComponents: []
                },] }
    ];
    return HeaderModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { HeaderComponent, HeaderModule, MaterialModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9oZWFkZXIvbGliL2hlYWRlci5jb21wb25lbnQudHMiLCJuZzovL2hlYWRlci9saWIvbWF0ZXJpYWwubW9kdWxlLnRzIiwibmc6Ly9oZWFkZXIvbGliL2hlYWRlci5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBFdmVudEVtaXR0ZXIsXG4gIE9uSW5pdCxcbiAgT3V0cHV0XG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuLy9pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG4vLyBpbXBvcnQgeyBUb2tlblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy90b2tlbi5zZXJ2aWNlJztcbi8vIGltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi4vbG9naW4vbG9naW4uY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWhlYWRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9oZWFkZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwdWJsaWMgaXNBZG1pbiA9IGZhbHNlO1xuICBwdWJsaWMgdXNlcm5hbWUgPSAnJztcbiAgQE91dHB1dCgpIGxvZ291dEVtaXR0ZXI6IEV2ZW50RW1pdHRlcjxib29sZWFuPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICAvL3ByaXZhdGUgYXV0aDogQXV0aFNlcnZpY2UsXG4gICAgLy8gcHJpdmF0ZSBtZTogVG9rZW5TZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnVwZGF0ZVVzZXJuYW1lKCk7XG4gIH1cblxuXG4gIHB1YmxpYyB1cGRhdGVVc2VybmFtZSgpIHtcbiAgICAvKnRoaXMuYXV0aC5nZXRVc2VyKCkuc3Vic2NyaWJlKHJlcyA9PiB7XG4gICAgICBpZiAocmVzKSB7XG4gICAgICAgIHRoaXMudXNlcm5hbWUgPSByZXNbJ3ByaW5jaXBhbCddWyduYW1lJ107XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gJyc7XG4gICAgICB9XG4gICAgfSk7Ki9cbiAgfVxuXG4gIC8vIG1vdmUgdG8gYXV0aCBzZXJ2aWNlXG4gIHB1YmxpYyBsb2dvdXQoKSB7XG4gICAgLypsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnYXV0aCcpO1xuICAgIHRoaXMudXNlcm5hbWUgPSAnJztcbiAgICAvLyB0aGlzLmxvZ291dEVtaXR0ZXIuZW1pdCh0cnVlKTtcbiAgICB0aGlzLmF1dGgubG9nb3V0KCk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7Ki9cbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgIE1hdEJ1dHRvblRvZ2dsZU1vZHVsZSxcbiAgICBNYXRDYXJkTW9kdWxlLFxuICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgIE1hdENoaXBzTW9kdWxlLFxuICAgIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gICAgTWF0RGlhbG9nTW9kdWxlLFxuICAgIE1hdEV4cGFuc2lvbk1vZHVsZSxcbiAgICBNYXRHcmlkTGlzdE1vZHVsZSxcbiAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICBNYXRJY29uTW9kdWxlLFxuICAgIE1hdExpc3RNb2R1bGUsXG4gICAgTWF0TWVudU1vZHVsZSxcbiAgICBNYXROYXRpdmVEYXRlTW9kdWxlLFxuICAgIE1hdFByb2dyZXNzQmFyTW9kdWxlLFxuICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgICBNYXRSYWRpb01vZHVsZSxcbiAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgTWF0U2lkZW5hdk1vZHVsZSxcbiAgICBNYXRTbGlkZXJNb2R1bGUsXG4gICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgTWF0U25hY2tCYXJNb2R1bGUsXG4gICAgTWF0VGFic01vZHVsZSxcbiAgICBNYXRUb29sdGlwTW9kdWxlLFxuICAgIE1hdFRvb2xiYXJNb2R1bGUsXG4gICAgTWF0Rm9ybUZpZWxkTW9kdWxlXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIE1hdEF1dG9jb21wbGV0ZU1vZHVsZSxcbiAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgICAgICBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXG4gICAgICAgIE1hdENhcmRNb2R1bGUsXG4gICAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgICBNYXRDaGlwc01vZHVsZSxcbiAgICAgICAgTWF0RGF0ZXBpY2tlck1vZHVsZSxcbiAgICAgICAgTWF0RGlhbG9nTW9kdWxlLFxuICAgICAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gICAgICAgIE1hdEdyaWRMaXN0TW9kdWxlLFxuICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgTWF0TGlzdE1vZHVsZSxcbiAgICAgICAgTWF0TWVudU1vZHVsZSxcbiAgICAgICAgTWF0TmF0aXZlRGF0ZU1vZHVsZSxcbiAgICAgICAgTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXG4gICAgICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgICAgICAgTWF0UmFkaW9Nb2R1bGUsXG4gICAgICAgIE1hdFNlbGVjdE1vZHVsZSxcbiAgICAgICAgTWF0U2lkZW5hdk1vZHVsZSxcbiAgICAgICAgTWF0U2xpZGVyTW9kdWxlLFxuICAgICAgICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcbiAgICAgICAgTWF0U25hY2tCYXJNb2R1bGUsXG4gICAgICAgIE1hdFRhYnNNb2R1bGUsXG4gICAgICAgIE1hdFRvb2x0aXBNb2R1bGUsXG4gICAgICAgIE1hdFRvb2xiYXJNb2R1bGUsXG4gICAgICAgIE1hdEZvcm1GaWVsZE1vZHVsZVxuICAgIF0sXG4gICAgZXhwb3J0czogW1xuICAgICAgICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXG4gICAgICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICAgICAgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxuICAgICAgICBNYXRDYXJkTW9kdWxlLFxuICAgICAgICBNYXRDaGVja2JveE1vZHVsZSxcbiAgICAgICAgTWF0Q2hpcHNNb2R1bGUsXG4gICAgICAgIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gICAgICAgIE1hdERpYWxvZ01vZHVsZSxcbiAgICAgICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgICAgICBNYXRHcmlkTGlzdE1vZHVsZSxcbiAgICAgICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgIE1hdExpc3RNb2R1bGUsXG4gICAgICAgIE1hdE1lbnVNb2R1bGUsXG4gICAgICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXG4gICAgICAgIE1hdFByb2dyZXNzQmFyTW9kdWxlLFxuICAgICAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXG4gICAgICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgICAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgICAgIE1hdFNsaWRlck1vZHVsZSxcbiAgICAgICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgICAgICBNYXRUYWJzTW9kdWxlLFxuICAgICAgICBNYXRUb29sdGlwTW9kdWxlLFxuICAgICAgICBNYXRUb29sYmFyTW9kdWxlLFxuICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGVcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsTW9kdWxlIHt9IiwiXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEhlYWRlckNvbXBvbmVudCB9IGZyb20gJy4vaGVhZGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4vbWF0ZXJpYWwubW9kdWxlJztcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxuICAgICAgICBSb3V0ZXJNb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogWyBIZWFkZXJDb21wb25lbnQgXSxcbiAgICBleHBvcnRzOiBbIEhlYWRlckNvbXBvbmVudCBdLFxuICAgIHByb3ZpZGVyczogW10sXG4gICAgZW50cnlDb21wb25lbnRzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBIZWFkZXJNb2R1bGUge31cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtJQXdCRSx5QkFHVTtRQUFBLFdBQU0sR0FBTixNQUFNO3VCQVBDLEtBQUs7d0JBQ0osRUFBRTs2QkFDNkIsSUFBSSxZQUFZLEVBQUU7S0FNOUQ7Ozs7SUFFTCxrQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7S0FDdkI7Ozs7SUFHTSx3Q0FBYzs7Ozs7Ozs7Ozs7Ozs7O0lBV2QsZ0NBQU07Ozs7Ozs7Ozs7O2dCQWpDZCxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLDB6Q0FBc0M7O2lCQUV2Qzs7OztnQkFUUSxNQUFNOzs7Z0NBY1osTUFBTTs7MEJBdEJUOzs7Ozs7O0FDQUE7Ozs7Z0JBK0JDLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wscUJBQXFCO3dCQUNyQixlQUFlO3dCQUNmLHFCQUFxQjt3QkFDckIsYUFBYTt3QkFDYixpQkFBaUI7d0JBQ2pCLGNBQWM7d0JBQ2QsbUJBQW1CO3dCQUNuQixlQUFlO3dCQUNmLGtCQUFrQjt3QkFDbEIsaUJBQWlCO3dCQUNqQixjQUFjO3dCQUNkLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixhQUFhO3dCQUNiLG1CQUFtQjt3QkFDbkIsb0JBQW9CO3dCQUNwQix3QkFBd0I7d0JBQ3hCLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixnQkFBZ0I7d0JBQ2hCLGVBQWU7d0JBQ2Ysb0JBQW9CO3dCQUNwQixpQkFBaUI7d0JBQ2pCLGFBQWE7d0JBQ2IsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGtCQUFrQjtxQkFDckI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLHFCQUFxQjt3QkFDckIsZUFBZTt3QkFDZixxQkFBcUI7d0JBQ3JCLGFBQWE7d0JBQ2IsaUJBQWlCO3dCQUNqQixjQUFjO3dCQUNkLG1CQUFtQjt3QkFDbkIsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGlCQUFpQjt3QkFDakIsY0FBYzt3QkFDZCxhQUFhO3dCQUNiLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixtQkFBbUI7d0JBQ25CLG9CQUFvQjt3QkFDcEIsd0JBQXdCO3dCQUN4QixjQUFjO3dCQUNkLGVBQWU7d0JBQ2YsZ0JBQWdCO3dCQUNoQixlQUFlO3dCQUNmLG9CQUFvQjt3QkFDcEIsaUJBQWlCO3dCQUNqQixhQUFhO3dCQUNiLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixrQkFBa0I7cUJBQ3JCO2lCQUNKOzt5QkExRkQ7Ozs7Ozs7QUNDQTs7OztnQkFNQyxRQUFRLFNBQUM7b0JBQ04sT0FBTyxFQUFFO3dCQUNMLFlBQVk7d0JBQ1osY0FBYzt3QkFDZCxZQUFZO3FCQUNmO29CQUNELFlBQVksRUFBRSxDQUFFLGVBQWUsQ0FBRTtvQkFDakMsT0FBTyxFQUFFLENBQUUsZUFBZSxDQUFFO29CQUM1QixTQUFTLEVBQUUsRUFBRTtvQkFDYixlQUFlLEVBQUUsRUFBRTtpQkFDdEI7O3VCQWpCRDs7Ozs7Ozs7Ozs7Ozs7OyJ9