(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/router'), require('@angular/material'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('header', ['exports', '@angular/core', '@angular/router', '@angular/material', '@angular/common'], factory) :
    (factory((global.header = {}),global.ng.core,global.ng.router,global.ng.material,global.ng.common));
}(this, (function (exports,core,router,material,common) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var HeaderComponent = /** @class */ (function () {
        function HeaderComponent(router$$1) {
            this.router = router$$1;
            this.isAdmin = false;
            this.username = '';
            this.logoutEmitter = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        HeaderComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.updateUsername();
            };
        /**
         * @return {?}
         */
        HeaderComponent.prototype.updateUsername = /**
         * @return {?}
         */
            function () {
                /*this.auth.getUser().subscribe(res => {
                      if (res) {
                        this.username = res['principal']['name'];
                      } else {
                        this.username = '';
                      }
                    });*/
            };
        /**
         * @return {?}
         */
        HeaderComponent.prototype.logout = /**
         * @return {?}
         */
            function () {
                /*localStorage.removeItem('auth');
                    this.username = '';
                    // this.logoutEmitter.emit(true);
                    this.auth.logout();
                    this.router.navigate(['/login']);*/
            };
        HeaderComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'app-header',
                        template: "<section class=\"main-header\">\n  <h2 class=\"main-text\">Saphira<span class=\"main-subtext\">3.0</span></h2>\n  <span class=\"left-menu\">\n    <h4 class=\"user-name\">{{this.username}}</h4>\n    <p matTooltip=\"Connected\" class=\"mock-status\">\u2022</p>\n    <button class=\"menu-button\" matTooltip=\"Direct: 867-5309\" mat-icon-button>\n      <mat-icon>phone</mat-icon>\n    </button>\n    <button class=\"menu-button\" matTooltip=\"surveys@surcon.net\" mat-icon-button>\n      <mat-icon>email</mat-icon>\n    </button>\n    <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\">\n      <mat-icon>menu</mat-icon>\n    </button>\n    <mat-menu #primaryMenu=\"matMenu\">\n      <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Selection\n        </button>\n      </a>\n      <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Status\n        </button>\n      </a>\n      <br>\n      <button mat-menu-item>Account</button>\n      <button mat-menu-item>Admin View</button>\n      <button mat-menu-item (click)=\"logout()\">Logout</button>\n    </mat-menu>\n  </span>\n</section>\n",
                        styles: [".main-header{width:97vw;padding:0 1.5vw;height:45px;display:flex;justify-content:space-between;border-bottom:1px solid #333}.button-link{text-decoration:none}.left-menu{display:flex;align-items:center}.user-name{font-family:Roboto-Light;font-size:20px;font-weight:200;color:#333}.mock-status{margin:-22px 10px 0;color:green;font-size:60px;height:45px}.main-text{font-family:Roboto-Light;font-size:36px;font-weight:200;color:#333}.main-subtext{color:#777}.menu-button{transition-duration:.2s}.menu-button:hover{background-color:#ddd;opacity:.5;transition-duration:.15s}a{text-decoration:none}"]
                    }] }
        ];
        /** @nocollapse */
        HeaderComponent.ctorParameters = function () {
            return [
                { type: router.Router }
            ];
        };
        HeaderComponent.propDecorators = {
            logoutEmitter: [{ type: core.Output }]
        };
        return HeaderComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        MaterialModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            material.MatAutocompleteModule,
                            material.MatButtonModule,
                            material.MatButtonToggleModule,
                            material.MatCardModule,
                            material.MatCheckboxModule,
                            material.MatChipsModule,
                            material.MatDatepickerModule,
                            material.MatDialogModule,
                            material.MatExpansionModule,
                            material.MatGridListModule,
                            material.MatInputModule,
                            material.MatIconModule,
                            material.MatListModule,
                            material.MatMenuModule,
                            material.MatNativeDateModule,
                            material.MatProgressBarModule,
                            material.MatProgressSpinnerModule,
                            material.MatRadioModule,
                            material.MatSelectModule,
                            material.MatSidenavModule,
                            material.MatSliderModule,
                            material.MatSlideToggleModule,
                            material.MatSnackBarModule,
                            material.MatTabsModule,
                            material.MatTooltipModule,
                            material.MatToolbarModule,
                            material.MatFormFieldModule
                        ],
                        exports: [
                            material.MatAutocompleteModule,
                            material.MatButtonModule,
                            material.MatButtonToggleModule,
                            material.MatCardModule,
                            material.MatCheckboxModule,
                            material.MatChipsModule,
                            material.MatDatepickerModule,
                            material.MatDialogModule,
                            material.MatExpansionModule,
                            material.MatGridListModule,
                            material.MatInputModule,
                            material.MatIconModule,
                            material.MatListModule,
                            material.MatMenuModule,
                            material.MatNativeDateModule,
                            material.MatProgressBarModule,
                            material.MatProgressSpinnerModule,
                            material.MatRadioModule,
                            material.MatSelectModule,
                            material.MatSidenavModule,
                            material.MatSliderModule,
                            material.MatSlideToggleModule,
                            material.MatSnackBarModule,
                            material.MatTabsModule,
                            material.MatTooltipModule,
                            material.MatToolbarModule,
                            material.MatFormFieldModule
                        ]
                    },] }
        ];
        return MaterialModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var HeaderModule = /** @class */ (function () {
        function HeaderModule() {
        }
        HeaderModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            MaterialModule,
                            router.RouterModule
                        ],
                        declarations: [HeaderComponent],
                        exports: [HeaderComponent],
                        providers: [],
                        entryComponents: []
                    },] }
        ];
        return HeaderModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.HeaderComponent = HeaderComponent;
    exports.HeaderModule = HeaderModule;
    exports.MaterialModule = MaterialModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vaGVhZGVyL2xpYi9oZWFkZXIuY29tcG9uZW50LnRzIiwibmc6Ly9oZWFkZXIvbGliL21hdGVyaWFsLm1vZHVsZS50cyIsIm5nOi8vaGVhZGVyL2xpYi9oZWFkZXIubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgRXZlbnRFbWl0dGVyLFxuICBPbkluaXQsXG4gIE91dHB1dFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbi8vaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuLy8gaW1wb3J0IHsgVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdG9rZW4uc2VydmljZSc7XG4vLyBpbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gJy4uL2xvZ2luL2xvZ2luLmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1oZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaGVhZGVyLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHVibGljIGlzQWRtaW4gPSBmYWxzZTtcbiAgcHVibGljIHVzZXJuYW1lID0gJyc7XG4gIEBPdXRwdXQoKSBsb2dvdXRFbWl0dGVyOiBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgLy9wcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlLFxuICAgIC8vIHByaXZhdGUgbWU6IFRva2VuU2VydmljZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy51cGRhdGVVc2VybmFtZSgpO1xuICB9XG5cblxuICBwdWJsaWMgdXBkYXRlVXNlcm5hbWUoKSB7XG4gICAgLyp0aGlzLmF1dGguZ2V0VXNlcigpLnN1YnNjcmliZShyZXMgPT4ge1xuICAgICAgaWYgKHJlcykge1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gcmVzWydwcmluY2lwYWwnXVsnbmFtZSddO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy51c2VybmFtZSA9ICcnO1xuICAgICAgfVxuICAgIH0pOyovXG4gIH1cblxuICAvLyBtb3ZlIHRvIGF1dGggc2VydmljZVxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIC8qbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2F1dGgnKTtcbiAgICB0aGlzLnVzZXJuYW1lID0gJyc7XG4gICAgLy8gdGhpcy5sb2dvdXRFbWl0dGVyLmVtaXQodHJ1ZSk7XG4gICAgdGhpcy5hdXRoLmxvZ291dCgpO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pOyovXG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gICAgTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxuICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXG4gICAgTWF0Q2FyZE1vZHVsZSxcbiAgICBNYXRDaGVja2JveE1vZHVsZSxcbiAgICBNYXRDaGlwc01vZHVsZSxcbiAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxuICAgIE1hdERpYWxvZ01vZHVsZSxcbiAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gICAgTWF0R3JpZExpc3RNb2R1bGUsXG4gICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBNYXRMaXN0TW9kdWxlLFxuICAgIE1hdE1lbnVNb2R1bGUsXG4gICAgTWF0TmF0aXZlRGF0ZU1vZHVsZSxcbiAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXG4gICAgTWF0UmFkaW9Nb2R1bGUsXG4gICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgTWF0U2xpZGVyTW9kdWxlLFxuICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxuICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgIE1hdFRhYnNNb2R1bGUsXG4gICAgTWF0VG9vbHRpcE1vZHVsZSxcbiAgICBNYXRUb29sYmFyTW9kdWxlLFxuICAgIE1hdEZvcm1GaWVsZE1vZHVsZVxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXG4gICAgICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICAgICAgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxuICAgICAgICBNYXRDYXJkTW9kdWxlLFxuICAgICAgICBNYXRDaGVja2JveE1vZHVsZSxcbiAgICAgICAgTWF0Q2hpcHNNb2R1bGUsXG4gICAgICAgIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gICAgICAgIE1hdERpYWxvZ01vZHVsZSxcbiAgICAgICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgICAgICBNYXRHcmlkTGlzdE1vZHVsZSxcbiAgICAgICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgIE1hdExpc3RNb2R1bGUsXG4gICAgICAgIE1hdE1lbnVNb2R1bGUsXG4gICAgICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXG4gICAgICAgIE1hdFByb2dyZXNzQmFyTW9kdWxlLFxuICAgICAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXG4gICAgICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgICAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgICAgIE1hdFNsaWRlck1vZHVsZSxcbiAgICAgICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgICAgICBNYXRUYWJzTW9kdWxlLFxuICAgICAgICBNYXRUb29sdGlwTW9kdWxlLFxuICAgICAgICBNYXRUb29sYmFyTW9kdWxlLFxuICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGVcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxuICAgICAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgICAgIE1hdEJ1dHRvblRvZ2dsZU1vZHVsZSxcbiAgICAgICAgTWF0Q2FyZE1vZHVsZSxcbiAgICAgICAgTWF0Q2hlY2tib3hNb2R1bGUsXG4gICAgICAgIE1hdENoaXBzTW9kdWxlLFxuICAgICAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxuICAgICAgICBNYXREaWFsb2dNb2R1bGUsXG4gICAgICAgIE1hdEV4cGFuc2lvbk1vZHVsZSxcbiAgICAgICAgTWF0R3JpZExpc3RNb2R1bGUsXG4gICAgICAgIE1hdElucHV0TW9kdWxlLFxuICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICBNYXRMaXN0TW9kdWxlLFxuICAgICAgICBNYXRNZW51TW9kdWxlLFxuICAgICAgICBNYXROYXRpdmVEYXRlTW9kdWxlLFxuICAgICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgICAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLFxuICAgICAgICBNYXRSYWRpb01vZHVsZSxcbiAgICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgICBNYXRTaWRlbmF2TW9kdWxlLFxuICAgICAgICBNYXRTbGlkZXJNb2R1bGUsXG4gICAgICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxuICAgICAgICBNYXRTbmFja0Jhck1vZHVsZSxcbiAgICAgICAgTWF0VGFic01vZHVsZSxcbiAgICAgICAgTWF0VG9vbHRpcE1vZHVsZSxcbiAgICAgICAgTWF0VG9vbGJhck1vZHVsZSxcbiAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBNYXRlcmlhbE1vZHVsZSB7fSIsIlxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBIZWFkZXJDb21wb25lbnQgfSBmcm9tICcuL2hlYWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuL21hdGVyaWFsLm1vZHVsZSc7XG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICBNYXRlcmlhbE1vZHVsZSxcbiAgICAgICAgUm91dGVyTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFsgSGVhZGVyQ29tcG9uZW50IF0sXG4gICAgZXhwb3J0czogWyBIZWFkZXJDb21wb25lbnQgXSxcbiAgICBwcm92aWRlcnM6IFtdLFxuICAgIGVudHJ5Q29tcG9uZW50czogW11cbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyTW9kdWxlIHt9XG4iXSwibmFtZXMiOlsicm91dGVyIiwiRXZlbnRFbWl0dGVyIiwiQ29tcG9uZW50IiwiUm91dGVyIiwiT3V0cHV0IiwiTmdNb2R1bGUiLCJNYXRBdXRvY29tcGxldGVNb2R1bGUiLCJNYXRCdXR0b25Nb2R1bGUiLCJNYXRCdXR0b25Ub2dnbGVNb2R1bGUiLCJNYXRDYXJkTW9kdWxlIiwiTWF0Q2hlY2tib3hNb2R1bGUiLCJNYXRDaGlwc01vZHVsZSIsIk1hdERhdGVwaWNrZXJNb2R1bGUiLCJNYXREaWFsb2dNb2R1bGUiLCJNYXRFeHBhbnNpb25Nb2R1bGUiLCJNYXRHcmlkTGlzdE1vZHVsZSIsIk1hdElucHV0TW9kdWxlIiwiTWF0SWNvbk1vZHVsZSIsIk1hdExpc3RNb2R1bGUiLCJNYXRNZW51TW9kdWxlIiwiTWF0TmF0aXZlRGF0ZU1vZHVsZSIsIk1hdFByb2dyZXNzQmFyTW9kdWxlIiwiTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIiwiTWF0UmFkaW9Nb2R1bGUiLCJNYXRTZWxlY3RNb2R1bGUiLCJNYXRTaWRlbmF2TW9kdWxlIiwiTWF0U2xpZGVyTW9kdWxlIiwiTWF0U2xpZGVUb2dnbGVNb2R1bGUiLCJNYXRTbmFja0Jhck1vZHVsZSIsIk1hdFRhYnNNb2R1bGUiLCJNYXRUb29sdGlwTW9kdWxlIiwiTWF0VG9vbGJhck1vZHVsZSIsIk1hdEZvcm1GaWVsZE1vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIlJvdXRlck1vZHVsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO1FBd0JFLHlCQUdVQTtZQUFBLFdBQU0sR0FBTkEsU0FBTTsyQkFQQyxLQUFLOzRCQUNKLEVBQUU7aUNBQzZCLElBQUlDLGlCQUFZLEVBQUU7U0FNOUQ7Ozs7UUFFTCxrQ0FBUTs7O1lBQVI7Z0JBQ0UsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3ZCOzs7O1FBR00sd0NBQWM7Ozs7Ozs7Ozs7Ozs7OztRQVdkLGdDQUFNOzs7Ozs7Ozs7OztvQkFqQ2RDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsMHpDQUFzQzs7cUJBRXZDOzs7Ozt3QkFUUUMsYUFBTTs7OztvQ0FjWkMsV0FBTTs7OEJBdEJUOzs7Ozs7O0FDQUE7Ozs7b0JBK0JDQyxhQUFRLFNBQUM7d0JBQ04sT0FBTyxFQUFFOzRCQUNMQyw4QkFBcUI7NEJBQ3JCQyx3QkFBZTs0QkFDZkMsOEJBQXFCOzRCQUNyQkMsc0JBQWE7NEJBQ2JDLDBCQUFpQjs0QkFDakJDLHVCQUFjOzRCQUNkQyw0QkFBbUI7NEJBQ25CQyx3QkFBZTs0QkFDZkMsMkJBQWtCOzRCQUNsQkMsMEJBQWlCOzRCQUNqQkMsdUJBQWM7NEJBQ2RDLHNCQUFhOzRCQUNiQyxzQkFBYTs0QkFDYkMsc0JBQWE7NEJBQ2JDLDRCQUFtQjs0QkFDbkJDLDZCQUFvQjs0QkFDcEJDLGlDQUF3Qjs0QkFDeEJDLHVCQUFjOzRCQUNkQyx3QkFBZTs0QkFDZkMseUJBQWdCOzRCQUNoQkMsd0JBQWU7NEJBQ2ZDLDZCQUFvQjs0QkFDcEJDLDBCQUFpQjs0QkFDakJDLHNCQUFhOzRCQUNiQyx5QkFBZ0I7NEJBQ2hCQyx5QkFBZ0I7NEJBQ2hCQywyQkFBa0I7eUJBQ3JCO3dCQUNELE9BQU8sRUFBRTs0QkFDTDFCLDhCQUFxQjs0QkFDckJDLHdCQUFlOzRCQUNmQyw4QkFBcUI7NEJBQ3JCQyxzQkFBYTs0QkFDYkMsMEJBQWlCOzRCQUNqQkMsdUJBQWM7NEJBQ2RDLDRCQUFtQjs0QkFDbkJDLHdCQUFlOzRCQUNmQywyQkFBa0I7NEJBQ2xCQywwQkFBaUI7NEJBQ2pCQyx1QkFBYzs0QkFDZEMsc0JBQWE7NEJBQ2JDLHNCQUFhOzRCQUNiQyxzQkFBYTs0QkFDYkMsNEJBQW1COzRCQUNuQkMsNkJBQW9COzRCQUNwQkMsaUNBQXdCOzRCQUN4QkMsdUJBQWM7NEJBQ2RDLHdCQUFlOzRCQUNmQyx5QkFBZ0I7NEJBQ2hCQyx3QkFBZTs0QkFDZkMsNkJBQW9COzRCQUNwQkMsMEJBQWlCOzRCQUNqQkMsc0JBQWE7NEJBQ2JDLHlCQUFnQjs0QkFDaEJDLHlCQUFnQjs0QkFDaEJDLDJCQUFrQjt5QkFDckI7cUJBQ0o7OzZCQTFGRDs7Ozs7OztBQ0NBOzs7O29CQU1DM0IsYUFBUSxTQUFDO3dCQUNOLE9BQU8sRUFBRTs0QkFDTDRCLG1CQUFZOzRCQUNaLGNBQWM7NEJBQ2RDLG1CQUFZO3lCQUNmO3dCQUNELFlBQVksRUFBRSxDQUFFLGVBQWUsQ0FBRTt3QkFDakMsT0FBTyxFQUFFLENBQUUsZUFBZSxDQUFFO3dCQUM1QixTQUFTLEVBQUUsRUFBRTt3QkFDYixlQUFlLEVBQUUsRUFBRTtxQkFDdEI7OzJCQWpCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==