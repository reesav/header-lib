import { EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
export declare class HeaderComponent implements OnInit {
    private router;
    isAdmin: boolean;
    username: string;
    logoutEmitter: EventEmitter<boolean>;
    constructor(router: Router);
    ngOnInit(): void;
    updateUsername(): void;
    logout(): void;
}
