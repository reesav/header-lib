/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
export class HeaderComponent {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        this.isAdmin = false;
        this.username = '';
        this.logoutEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.updateUsername();
    }
    /**
     * @return {?}
     */
    updateUsername() {
        /*this.auth.getUser().subscribe(res => {
              if (res) {
                this.username = res['principal']['name'];
              } else {
                this.username = '';
              }
            });*/
    }
    /**
     * @return {?}
     */
    logout() {
        /*localStorage.removeItem('auth');
            this.username = '';
            // this.logoutEmitter.emit(true);
            this.auth.logout();
            this.router.navigate(['/login']);*/
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-header',
                template: "<section class=\"main-header\">\n  <h2 class=\"main-text\">Saphira<span class=\"main-subtext\">3.0</span></h2>\n  <span class=\"left-menu\">\n    <h4 class=\"user-name\">{{this.username}}</h4>\n    <p matTooltip=\"Connected\" class=\"mock-status\">\u2022</p>\n    <button class=\"menu-button\" matTooltip=\"Direct: 867-5309\" mat-icon-button>\n      <mat-icon>phone</mat-icon>\n    </button>\n    <button class=\"menu-button\" matTooltip=\"surveys@surcon.net\" mat-icon-button>\n      <mat-icon>email</mat-icon>\n    </button>\n    <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\">\n      <mat-icon>menu</mat-icon>\n    </button>\n    <mat-menu #primaryMenu=\"matMenu\">\n      <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Selection\n        </button>\n      </a>\n      <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Status\n        </button>\n      </a>\n      <br>\n      <button mat-menu-item>Account</button>\n      <button mat-menu-item>Admin View</button>\n      <button mat-menu-item (click)=\"logout()\">Logout</button>\n    </mat-menu>\n  </span>\n</section>\n",
                styles: [".main-header{width:97vw;padding:0 1.5vw;height:45px;display:flex;justify-content:space-between;border-bottom:1px solid #333}.button-link{text-decoration:none}.left-menu{display:flex;align-items:center}.user-name{font-family:Roboto-Light;font-size:20px;font-weight:200;color:#333}.mock-status{margin:-22px 10px 0;color:green;font-size:60px;height:45px}.main-text{font-family:Roboto-Light;font-size:36px;font-weight:200;color:#333}.main-subtext{color:#777}.menu-button{transition-duration:.2s}.menu-button:hover{background-color:#ddd;opacity:.5;transition-duration:.15s}a{text-decoration:none}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [
    { type: Router }
];
HeaderComponent.propDecorators = {
    logoutEmitter: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.isAdmin;
    /** @type {?} */
    HeaderComponent.prototype.username;
    /** @type {?} */
    HeaderComponent.prototype.logoutEmitter;
    /** @type {?} */
    HeaderComponent.prototype.router;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hlYWRlci8iLCJzb3VyY2VzIjpbImxpYi9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFlBQVksRUFFWixNQUFNLEVBQ1AsTUFBTSxlQUFlLENBQUM7QUFHdkIsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBVXpDLE1BQU07Ozs7SUFNSixZQUdVO1FBQUEsV0FBTSxHQUFOLE1BQU07dUJBUEMsS0FBSzt3QkFDSixFQUFFOzZCQUM2QixJQUFJLFlBQVksRUFBRTtLQU05RDs7OztJQUVMLFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7S0FDdkI7Ozs7SUFHTSxjQUFjOzs7Ozs7Ozs7Ozs7SUFXZCxNQUFNOzs7Ozs7Ozs7WUFqQ2QsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QiwwekNBQXNDOzthQUV2Qzs7OztZQVRRLE1BQU07Ozs0QkFjWixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBFdmVudEVtaXR0ZXIsXG4gIE9uSW5pdCxcbiAgT3V0cHV0XG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuLy9pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG4vLyBpbXBvcnQgeyBUb2tlblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy90b2tlbi5zZXJ2aWNlJztcbi8vIGltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi4vbG9naW4vbG9naW4uY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWhlYWRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9oZWFkZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwdWJsaWMgaXNBZG1pbiA9IGZhbHNlO1xuICBwdWJsaWMgdXNlcm5hbWUgPSAnJztcbiAgQE91dHB1dCgpIGxvZ291dEVtaXR0ZXI6IEV2ZW50RW1pdHRlcjxib29sZWFuPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICAvL3ByaXZhdGUgYXV0aDogQXV0aFNlcnZpY2UsXG4gICAgLy8gcHJpdmF0ZSBtZTogVG9rZW5TZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnVwZGF0ZVVzZXJuYW1lKCk7XG4gIH1cblxuXG4gIHB1YmxpYyB1cGRhdGVVc2VybmFtZSgpIHtcbiAgICAvKnRoaXMuYXV0aC5nZXRVc2VyKCkuc3Vic2NyaWJlKHJlcyA9PiB7XG4gICAgICBpZiAocmVzKSB7XG4gICAgICAgIHRoaXMudXNlcm5hbWUgPSByZXNbJ3ByaW5jaXBhbCddWyduYW1lJ107XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gJyc7XG4gICAgICB9XG4gICAgfSk7Ki9cbiAgfVxuXG4gIC8vIG1vdmUgdG8gYXV0aCBzZXJ2aWNlXG4gIHB1YmxpYyBsb2dvdXQoKSB7XG4gICAgLypsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnYXV0aCcpO1xuICAgIHRoaXMudXNlcm5hbWUgPSAnJztcbiAgICAvLyB0aGlzLmxvZ291dEVtaXR0ZXIuZW1pdCh0cnVlKTtcbiAgICB0aGlzLmF1dGgubG9nb3V0KCk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7Ki9cbiAgfVxuXG59XG4iXX0=