/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
        this.isAdmin = false;
        this.username = '';
        this.logoutEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.updateUsername();
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.updateUsername = /**
     * @return {?}
     */
    function () {
        /*this.auth.getUser().subscribe(res => {
              if (res) {
                this.username = res['principal']['name'];
              } else {
                this.username = '';
              }
            });*/
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.logout = /**
     * @return {?}
     */
    function () {
        /*localStorage.removeItem('auth');
            this.username = '';
            // this.logoutEmitter.emit(true);
            this.auth.logout();
            this.router.navigate(['/login']);*/
    };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-header',
                    template: "<section class=\"main-header\">\n  <h2 class=\"main-text\">Saphira<span class=\"main-subtext\">3.0</span></h2>\n  <span class=\"left-menu\">\n    <h4 class=\"user-name\">{{this.username}}</h4>\n    <p matTooltip=\"Connected\" class=\"mock-status\">\u2022</p>\n    <button class=\"menu-button\" matTooltip=\"Direct: 867-5309\" mat-icon-button>\n      <mat-icon>phone</mat-icon>\n    </button>\n    <button class=\"menu-button\" matTooltip=\"surveys@surcon.net\" mat-icon-button>\n      <mat-icon>email</mat-icon>\n    </button>\n    <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\">\n      <mat-icon>menu</mat-icon>\n    </button>\n    <mat-menu #primaryMenu=\"matMenu\">\n      <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Selection\n        </button>\n      </a>\n      <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n        <button mat-menu-item class=\"button-link\">\n          Wellbore Status\n        </button>\n      </a>\n      <br>\n      <button mat-menu-item>Account</button>\n      <button mat-menu-item>Admin View</button>\n      <button mat-menu-item (click)=\"logout()\">Logout</button>\n    </mat-menu>\n  </span>\n</section>\n",
                    styles: [".main-header{width:97vw;padding:0 1.5vw;height:45px;display:flex;justify-content:space-between;border-bottom:1px solid #333}.button-link{text-decoration:none}.left-menu{display:flex;align-items:center}.user-name{font-family:Roboto-Light;font-size:20px;font-weight:200;color:#333}.mock-status{margin:-22px 10px 0;color:green;font-size:60px;height:45px}.main-text{font-family:Roboto-Light;font-size:36px;font-weight:200;color:#333}.main-subtext{color:#777}.menu-button{transition-duration:.2s}.menu-button:hover{background-color:#ddd;opacity:.5;transition-duration:.15s}a{text-decoration:none}"]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    HeaderComponent.propDecorators = {
        logoutEmitter: [{ type: Output }]
    };
    return HeaderComponent;
}());
export { HeaderComponent };
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.isAdmin;
    /** @type {?} */
    HeaderComponent.prototype.username;
    /** @type {?} */
    HeaderComponent.prototype.logoutEmitter;
    /** @type {?} */
    HeaderComponent.prototype.router;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hlYWRlci8iLCJzb3VyY2VzIjpbImxpYi9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFlBQVksRUFFWixNQUFNLEVBQ1AsTUFBTSxlQUFlLENBQUM7QUFHdkIsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDOztJQWdCdkMseUJBR1U7UUFBQSxXQUFNLEdBQU4sTUFBTTt1QkFQQyxLQUFLO3dCQUNKLEVBQUU7NkJBQzZCLElBQUksWUFBWSxFQUFFO0tBTTlEOzs7O0lBRUwsa0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0tBQ3ZCOzs7O0lBR00sd0NBQWM7Ozs7Ozs7Ozs7Ozs7OztJQVdkLGdDQUFNOzs7Ozs7Ozs7OztnQkFqQ2QsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QiwwekNBQXNDOztpQkFFdkM7Ozs7Z0JBVFEsTUFBTTs7O2dDQWNaLE1BQU07OzBCQXRCVDs7U0FrQmEsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgRXZlbnRFbWl0dGVyLFxuICBPbkluaXQsXG4gIE91dHB1dFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbi8vaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuLy8gaW1wb3J0IHsgVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdG9rZW4uc2VydmljZSc7XG4vLyBpbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gJy4uL2xvZ2luL2xvZ2luLmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1oZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaGVhZGVyLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHVibGljIGlzQWRtaW4gPSBmYWxzZTtcbiAgcHVibGljIHVzZXJuYW1lID0gJyc7XG4gIEBPdXRwdXQoKSBsb2dvdXRFbWl0dGVyOiBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgLy9wcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlLFxuICAgIC8vIHByaXZhdGUgbWU6IFRva2VuU2VydmljZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy51cGRhdGVVc2VybmFtZSgpO1xuICB9XG5cblxuICBwdWJsaWMgdXBkYXRlVXNlcm5hbWUoKSB7XG4gICAgLyp0aGlzLmF1dGguZ2V0VXNlcigpLnN1YnNjcmliZShyZXMgPT4ge1xuICAgICAgaWYgKHJlcykge1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gcmVzWydwcmluY2lwYWwnXVsnbmFtZSddO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy51c2VybmFtZSA9ICcnO1xuICAgICAgfVxuICAgIH0pOyovXG4gIH1cblxuICAvLyBtb3ZlIHRvIGF1dGggc2VydmljZVxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIC8qbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2F1dGgnKTtcbiAgICB0aGlzLnVzZXJuYW1lID0gJyc7XG4gICAgLy8gdGhpcy5sb2dvdXRFbWl0dGVyLmVtaXQodHJ1ZSk7XG4gICAgdGhpcy5hdXRoLmxvZ291dCgpO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pOyovXG4gIH1cblxufVxuIl19